# comment out nocows in ansible.cfg and run

```
ansible-playbook -i inventory/myhosts plays/penguins-and-dragons.yml
```

# test sudoers settings with -b (--become) (needs nginx installed)

```
ansible-playbook -i inventory/myhosts plays/nginx-service-test.yml -b
```

# docker stuff, WIP

https://stackoverflow.com/questions/44962282/how-to-write-an-ansible-playbook-with-docker-compose

```
ansible-playbook -i inventory/myhosts plays/docker-test.yml
```

